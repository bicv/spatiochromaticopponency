function LinMaps=PoolingPatterns()

% function LinMaps=PoolingPatterns();
% This function creates the specific poolers that were found to work well 
% for the descriptors tested in ECCV paper "Spatio-Chromatic Opponenent 
% Features", ECCV 2014, Part V, LNCS, pp 81.95.
% 
% Returns a predefined 17x17x16 array containing pooling maps over a 
% 17x17 region of image space.  16 different maps are created.
% 
% Inputs: None
% 
% Output: LinMaps - 17x17x16 array. The ith pooler is merely LinMaps(:,:,i)

%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                                                              
% %              (c)  B I C V   G R O U P  2 0 1 4                      % %                                                             
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % W A R N I N G:    THIS CODE IS INTENDED FOR RESEARCH USE; IT IS       %
% % UNOPTIMISED, UNTESTED AND MAINTAINED ONLY THROUGH BEST EFFORTS.       %
% % THE BICV GROUP MEMBERS AND/OR IMPERIAL COLLEGE  ARE NOT LIABLE        %
% % FOR LOSS, INJURY OR OTHER HARM RESULTING FROM USE OF THIS CODE.       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% %                                                                     % %
% % Principal Author: I Alexiou, 2014 (revised)                                        % %
% % Last Modification:  AAB (comments and tidying)                                                % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% 
%
% 

% Parameters used in the paper (see above)
Diameter = 17;
R1 = 0.45;
R2 = 0.6;
VF = 20; % For high-quality visualisation, only
% End main parameter definitions

% A high-res version of the map
Map = PoolingPatternsGenerator(Diameter*VF,R1,R2); % Used also to visualise

% Normal-sized version
LinMaps = imresize(Map,[Diameter Diameter],'bilinear');

% Normalisation
LinMaps = LinMaps ./ repmat(sum(sum(LinMaps)),[Diameter,Diameter,1]);

function Sub=PoolingPatternsGenerator(Diameter,r1,r2 ) %
% Private function: the main workhorse for definition

% Set up NRows and NCols
NRow=Diameter; NCol=Diameter;

Sub = zeros(NRow,NCol,16);

rho = [r1 r2];

[x,y]=meshgrid(-NCol/2:(NCol/2-1),-NRow/2:(NRow/2-1));

radius = sqrt( (2*(x+0.5)/NCol).^2 + (2*(y+0.5)/NRow).^2 );

i=1;

radius=1.4142-radius;

pow=4;  

Center = exp(-log(radius/(1.412-rho(1))).^pow / 0.3 ); 

radius=1.4142-radius;

% Handle the inner (non-central) poolers
d=1;

for orientations = 0:pi/4:7*pi/4

    theta = atan2(-y,x);              

    sintheta = sin(theta);
    costheta = cos(theta);

    ds = sintheta * cos(orientations) - costheta * sin(orientations);    
    dc = costheta * cos(orientations) + sintheta * sin(orientations);    

    dtheta = abs(atan2(ds,dc));                          

    Angular = exp( -dtheta.^pow /5);
                                                       
    Sub(:,:,i) = (Angular .* Center);
    i=i+1;

end

% Handle the outer poolers
d=2;
Radial = exp(-log(radius/rho(d)).^pow / 0.5); 

% it can Re-use Angular for speed. It is used only once to generate the
% maps so it does affect the speed

for orientations = 0:pi/4:7*pi/4

    theta = atan2(-y,x);              

    sintheta = sin(theta);
    costheta = cos(theta);

    ds = sintheta * cos(orientations) - costheta * sin(orientations);    
    dc = costheta * cos(orientations) + sintheta * sin(orientations);    

    dtheta = abs(atan2(ds,dc));                          

    Angular = exp( -dtheta.^pow /5);
                                                       
    Sub(:,:,i) = (Angular .* Radial);
    i=i+1;

end

