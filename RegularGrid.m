function [Grid]=RegularGrid(OppDogArray,step,BorderOffsets)

% function [Grid,BorderOffsets]=RegularGrid(OppDoGArray,step,BorderOffsets)
% This function creates the sampling grids used in descriptor generation 
% for the descriptors tested in ECCV paper "Spatio-Chromatic Opponenent 
% Features", ECCV 2014, Part V, LNCS, pp 81.95.
% 
% 
% Inputs:
%       OppDogArray   -  2D array over which grid will be provided
%       step          -  Stride length
%       BorderOffsets -  Border span to avoid
%      
%
% Outputs:
%       Grid          - Grid coordinates for sampling as a cell array
% 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                                                              
% %              (c)  B I C V   G R O U P  2 0 1 4                      % %                                                             
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % W A R N I N G:    THIS CODE IS INTENDED FOR RESEARCH USE; IT IS       %
% % UNOPTIMISED, UNTESTED AND MAINTAINED ONLY THROUGH BEST EFFORTS.       %
% % THE BICV GROUP MEMBERS AND/OR IMPERIAL COLLEGE  ARE NOT LIABLE        %
% % FOR LOSS, INJURY OR OTHER HARM RESULTING FROM USE OF THIS CODE.       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% %                                                                     % %
% % Principal Author: I. Alexiou                                        % %
% % Last Modification:                                                  % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %


% Get size of input (assumes this is a 2D array)

[y,x]=size(OppDogArray);


dy=1:step:y;

dx=1:step:x;


Grid=cell(2,length(BorderOffsets));

for bo=1:length(BorderOffsets)
    
    indy = dy>BorderOffsets(bo) & dy<(y-BorderOffsets(bo));

    indx = dx>BorderOffsets(bo) & dx<(x-BorderOffsets(bo));

    % Border handling
    Grid(1,bo)={repmat(dy(indy)',1,length(dx(indx)))};
    Grid(2,bo)={repmat(dx(indx),length(dy(indy)),1)};
    
end

