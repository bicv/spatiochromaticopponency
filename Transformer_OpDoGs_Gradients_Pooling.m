function OpDoGFeatures=Transformer_OpDoGs_Gradients_Pooling(I,...
    Shifted_Derivative,PoolingMappings,OppChoice)

% function OpDoG_Features=Transformer_OpDoGs_Gradients_Pooling(I,...
%    Shifted_Derivative,PoolingMappings,OppChoice)
% 
% Generates spatio-chromatic opponent descriptors on a dense grid following 
% the ECCV paper (Alexiou & Bharath) "Spatio-Chromatic Opponent Features",
% ECCV 2014, Part V, LNCS, pp 81.95. Use for research purposes is permitted
% providing you cite the paper in your work.
%
% Inputs:
%                 I         -   [MxNx3] RGB image
%      Shifted_Derivative   -   9x9x4 double array of shifted derivs
%      PoolingMappings      -   17x17x16 array of spatial poolers
%            OppChoice      -   Scalar integer in range 1 to 6
%
% Output:
%           OpDoGFeatures   -   Approx [(M-BI)*(N-BI)/SS] x 128 matrix of
%                               densely sampled descriptors; BI:
%                               BorderIgnore (see below) and SS: StepSize
%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                                                             
% %              (c)  B I C V   G R O U P  2 0 1 4                      % %                                                             
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % W A R N I N G:    THIS CODE IS INTENDED FOR RESEARCH USE; IT IS       %
% % UNOPTIMISED, UNTESTED AND MAINTAINED ONLY THROUGH BEST EFFORTS.       %
% % THE BICV GROUP MEMBERS AND/OR IMPERIAL COLLEGE  ARE NOT LIABLE        %
% % FOR LOSS, INJURY OR OTHER HARM RESULTING FROM USE OF THIS CODE.       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% %                                                                     % %
% % Principal Author: I Alexiou, 2014 (revised)                         % %
% % Last Modification:  AAB (comments and tidying)                      % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% 
%
 
global HasWorkingGPU;

% Definition of the OpDoGs
% Weights=[Red,Green,Blue]; %  

% "Spatio-Chromatic Wavelength" definitions
Weights = [  1,-1, 0 ; % Red-Green
             1, 0,-1 ; % Red-Blue
             0, 1,-1 ; % Green-Blue
             2,-1,-1 ; % Red-Cyan
            -1, 2,-1 ; % Green-Magenta
            -1,-1, 2]; % Blue-Yellow

% Spatial definitions
Cent_Sur_Params(1).Sigma=0.5;    % Center -Gaussian sigma
Cent_Sur_Params(1).Size=[3,3];   % Center - Gaussian size
Cent_Sur_Params(2).Sigma=2;      % Surround - Gaussian sigma
Cent_Sur_Params(2).Size=[11,11]; % Surround - Gaussian size

% Center-Surround assignment to the colour layers in the order of RGB and
% aligned with the Weights above
CS_index =[1,2,1;
           1,1,2;
           1,1,2;
           1,2,2;
           2,1,2;
           2,2,1];


% Grid defninitions
StepSize = 3;
BorderIgnore = 9;

if exist('gpuArray','class') 
    if gpuDeviceCount > 0 
        HasWorkingGPU = 1;
    else
        HasWorkingGPU = 0;
    end
end

if HasWorkingGPU == 1
   LMs = gpuArray(single(PoolingMappings)) ;
   I = gpuArray(single(I));
else
    LMs = PoolingMappings;
end

if ndims(I) < 3
    disp([mfilename,': expecting a colour image (MxNx3 array).']); 
    error([mfilename,': bailing out']);
end

% Else (implict) assumes that the image is 3D, RGB colourspace.
Height = size(I,1); Width = size(I,2) ;

% Always in "Centre-Surround" format
% (Example): lets select Red-Green with Red and Green Surround
% CS_index =[1,2,1]; % This index assigns center-surround to the 
% opponent channel Red - Green
% Third CS_index(3) has 0 weight for R-G (no B)

OpDoG=Xdash_mode3(Weights(OppChoice,:),...
I(:,:,1),I(:,:,2),I(:,:,3),...
Cent_Sur_Params(CS_index(OppChoice,:))); 

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
% (Example): select Blue-Yellow with Blue center and Yellow Surround
% OppChoice =6; preselected outside do not uncomment this line
% CS_index(OppChoice,:) =[2,2,1];
% OpDoG=Xdash_mode3(Weights(OppChoice,:),...
%  I(:,:,1),I(:,:,2),I(:,:,3),...
% Cent_Sur_Params(CS_index(OppChoice,:))); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
% Now, apply gradient-like computation
GradientDirections=ShiftedGradientEncoding(OpDoG,Shifted_Derivative);

% Create the grids for pooling : Use Grid in case you go for Spatial
% Pyramids or other locatation depedent techniques
[Grid,Y,X,LinSize] = MakeGrids(OpDoG,StepSize,BorderIgnore);

% Get the sampled pooled responses
OpDoGFeatures = PoolingLayer(OpDoG,GradientDirections,LMs,LinSize,Y,X);

% Perform a local normalization
OpDoGFeatures =( OpDoGFeatures ./...
    repmat(sqrt(sum(OpDoGFeatures.^2,2))+eps,[1,size(OpDoGFeatures,2)]));



function [GridLin,Y,X,LinSize]=MakeGrids(OpDoG,step,border)

Grid = RegularGrid(zeros(size(OpDoG)),step,border); % step=3

Y = Grid{1}(:,1);
X = Grid{2}(1,:);

GridLin=[Grid{1}(:),Grid{2}(:)];

LinSize = size(Grid{1},1)*size(Grid{1},2);

function OpFeat = PoolingLayer(OpDoG,GradientDirections,LMap,LinSize,Y,X)
global HasWorkingGPU

NumAttr=16;

if HasWorkingGPU
    OpFeat = gpuArray.zeros(size(OpDoG,1),size(OpDoG,2),NumAttr*8,'single');
else
    OpFeat = zeros(size(OpDoG,1),size(OpDoG,2),NumAttr*8);
end

for attr=1:NumAttr
    
    OpFeat(:,:,(attr-1)*8+(1:8)) = imfilter(GradientDirections,LMap(:,:,attr),'symmetric','conv') ;
    
end


if HasWorkingGPU
    OpFeat = reshape(OpFeat(Y,X,:),LinSize,NumAttr*8);  
    OpFeat = gather(OpFeat);
else
    OpFeat = reshape(OpFeat(Y,X,:),LinSize,NumAttr*8);  
end

function GradientDirections=ShiftedGradientEncoding(OpDoG,Shifted_Derivative)
global HasWorkingGPU

if HasWorkingGPU
    GradientDirections = gpuArray.zeros(size(OpDoG,1),size(OpDoG,2),8,'single');
    Orientations = gpuArray.zeros(size(OpDoG,1),size(OpDoG,2),4,'single');
else
    GradientDirections = zeros(size(OpDoG,1),size(OpDoG,2),8);
    Orientations =  zeros(size(OpDoG,1),size(OpDoG,2),4);
end

for or=1:4, 
    Orientations(:,:,or)=conv2(OpDoG,Shifted_Derivative(:,:,or),'same'); 
end

for L=1:4, GradientDirections(:,:,L)=max(Orientations(:,:,L),0); end

for L=5:8, GradientDirections(:,:,L)=-min(Orientations(:,:,L-4),0); end

function OpDoG=Xdash_mode3(Weights,Red,Green,Blue,Cent_Sur_Params)

if Weights(1)==0, Red=0; 
    elseif Weights(2)==0, Green=0;
    elseif Weights(3)==0, Blue=0;
end

% Combination of convolution ops happens here....
OpDoG = +Weights(1)*conv2( Red ,fspecial('gauss', Cent_Sur_Params(1).Size , Cent_Sur_Params(1).Sigma),'same') ...
        +Weights(2)*conv2( Green ,fspecial('gauss', Cent_Sur_Params(2).Size , Cent_Sur_Params(2).Sigma),'same') ...
        +Weights(3)*conv2( Blue ,fspecial('gauss', Cent_Sur_Params(3).Size , Cent_Sur_Params(3).Sigma),'same') ;
