% Script spatiochromaticopptest
% 
% Script to demonstrate the calculation of spatiochromatic opponency
% channels as described in in ECCV paper (Alexiou & Bharath) "Spatio-
% Chromatic Opponent Features", ECCV 2014, Part V, LNCS, pp 81.95.
%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                                                             
% %              (c)  B I C V   G R O U P  2 0 1 4                      % %                                                             
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % W A R N I N G:    THIS CODE IS INTENDED FOR RESEARCH USE; IT IS       %
% % UNOPTIMISED, UNTESTED AND MAINTAINED ONLY THROUGH BEST EFFORTS.       %
% % THE BICV GROUP MEMBERS AND/OR IMPERIAL COLLEGE  ARE NOT LIABLE        %
% % FOR LOSS, INJURY OR OTHER HARM RESULTING FROM USE OF THIS CODE.       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% %                                                                     % %
% % Principal Author: I Alexiou, 2014 (revised)                         % %
% % Last Modification:  AAB (Comments, options)                         % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% 

% This code emonstrates how to produce various flavours of OpDoG Channes 
% using dense pixelwise computation followed by subsampling every 3 pixels.
% Fully dense sampling will be made an option in a later version of the
% code.

%Select Spatio-Chromatic Opponency

  OpDoG_Choice =   1 ; % Red-Green
% OpDoG_Choice =   2 ; % Red-Blue
% OpDoG_Choice =   3 ; % Green-Blue
% OpDoG_Choice =   4 ; % Red-Cyan
% OpDoG_Choice =   5 ; % Green-Magenta
% OpDoG_Choice =   6 ; % Blue-Yellow 
 
% Used for swithing between CPU and GPU code
global HasWorkingGPU;

% Initialize the Pooling Patterns and keep in Memory
PoolingMappings=PoolingPatterns;


% Load the filters for gradient encoding
load Shifted_Derivative;

% This should be present in most recent versions; if not, substitute with
% any RGB image.
I = double(imread('peppers.png')); % Usually bundled with Matlab

% Transformer_OpDoGs_Gradients_Pooling is the main function to try out... 
tic
OpDog_Features=Transformer_OpDoGs_Gradients_Pooling(I,Shifted_Derivative,...
    PoolingMappings,OpDoG_Choice);
disp(['Done']);
toc
if HasWorkingGPU 
    disp('GPU found and used.') 
else
    disp('No usable GPU found.'); 
end